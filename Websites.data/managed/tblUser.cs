﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Websites.data
{
    public partial class tblUser
    {
        
        public static IQueryable<tblUser> GetAllUser()
        {
           WorkWebsiteEntities db = new WorkWebsiteEntities();
            return db.tblUsers.Where(x => x.IsActive == true);
        }
        public static tblUser GetByUserNamePassword(string username,string password)
        {
            WorkWebsiteEntities db = new WorkWebsiteEntities();
            return db.tblUsers.FirstOrDefault(x => x.UserName == username && x.Password == password && x.IsActive == true);
        }
        public static tblUser GetUserByUserName(string username)
        {
            WorkWebsiteEntities db = new WorkWebsiteEntities();
            return db.tblUsers.FirstOrDefault(x => x.UserName == username  &&  x.IsActive == true);
        }
       
    }
}
