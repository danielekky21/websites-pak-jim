﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Websites.data
{
    public partial class tblServices
    {
        public static IQueryable<tblService> GetAllService()
        {
            WorkWebsiteEntities db = new WorkWebsiteEntities();
            return db.tblServices.Where(x => x.IsActive == true);

        }
        public static tblService GetServiceByID(Guid id)
        {
            WorkWebsiteEntities db = new WorkWebsiteEntities();
            return db.tblServices.FirstOrDefault(x => x.ServicesID == id && x.IsActive == true);
        }
    }
}
