﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Websites.data
{
    public partial class tblContact
    {
        public static IQueryable<tblContact> GetAllContact()
        {
            WorkWebsiteEntities db = new WorkWebsiteEntities();
            return db.tblContacts.Where(x => x.IsActive == true);
        }
        public static tblContact GetContactByID(Guid id)
        {
            WorkWebsiteEntities db = new WorkWebsiteEntities();
            return db.tblContacts.FirstOrDefault(x => x.ContactId == id && x.IsActive == true);
        }
    }
}
