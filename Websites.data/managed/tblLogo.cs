﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Websites.data
{
    public partial class tblLogo
    {
        public static IQueryable<tblLogo> GetAllLogo()
        {
            WorkWebsiteEntities db = new WorkWebsiteEntities();
            return db.tblLogoes;
        }
        public static tblLogo GetLogoByID(Guid id)
        {
            WorkWebsiteEntities db = new WorkWebsiteEntities();
            return db.tblLogoes.FirstOrDefault(x => x.IsActive == true && x.LogoId == id);  
        }
    }
}
