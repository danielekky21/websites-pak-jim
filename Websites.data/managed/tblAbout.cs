﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Websites.data
{
    public partial class tblAbout
    {
        public static IQueryable<tblAbout> GetAllAbout()
        {
            WorkWebsiteEntities db = new WorkWebsiteEntities();
            return db.tblAbouts.Where(x => x.IsActive == true);
        }
        public static tblAbout GetAboutByID(Guid id)
        {
            WorkWebsiteEntities db = new WorkWebsiteEntities();
            return db.tblAbouts.FirstOrDefault(x => x.IsActive == true && x.AboutID == id);
        }
    }
}
