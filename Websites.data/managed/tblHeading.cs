﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Websites.data
{
    public partial class tblHeading
    {
        public static IQueryable<tblHeading> GetAllHeadingData()
        {
            WorkWebsiteEntities db = new WorkWebsiteEntities();
            return db.tblHeadings.Where(x => x.IsActive == true);
        }
        public static tblHeading GetHeadingByID(Guid id)
        {
            WorkWebsiteEntities db = new WorkWebsiteEntities();
            return db.tblHeadings.FirstOrDefault(x => x.IsActive == true && x.HeadingID == id);
        }
        public static IQueryable<tblHeadingPicture> GetHeadingPicByHeadingId()
        {
            WorkWebsiteEntities db = new WorkWebsiteEntities();
            return db.tblHeadingPictures;
        }
        public static tblHeadingPicture GetHeadingPictByID(Guid id)
        {
            WorkWebsiteEntities db = new WorkWebsiteEntities();
            return db.tblHeadingPictures.FirstOrDefault(x => x.HeadingPictId == id);
        }
    }
}
