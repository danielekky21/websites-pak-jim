﻿using System;
using System.Collections.Generic;
using System.Linq;
using Websites.data;
using System.Web;
using System.Web.Mvc;

namespace Websites.Areas.Cms.Controllers
{
    [SessionTimeout]
    public partial class ContactController : Controller
    {
        // GET: Cms/Contact
        public virtual ActionResult Index()
        {
            var data = tblContact.GetAllContact().OrderByDescending(x => x.CreatedDate).ToList();
            ViewData.Model = data;
            return View();
        }
        public virtual ActionResult AddEdit(Guid ids)
        {
            if (ids != Guid.Empty)
            {
                var data = tblContact.GetContactByID(ids);
                if (data != null)
                {
                    ViewData.Model = data;
                }
            }
            else
            {

                ViewData.Model = new tblContact();
            }

            return View();
        }
        [HttpPost]
        public virtual ActionResult AddEdit(tblContact model)
        {
            WorkWebsiteEntities db = new WorkWebsiteEntities();
            if (model.ContactId != Guid.Empty)
            {
                var data = tblContact.GetContactByID(model.ContactId);
                if (data != null)
                {
                    data = model;
                    data.UpdatedBy = Session["User"].ToString();
                    data.UpdatedDate = DateTime.Now;
                    db.SaveChanges();
                }
                
            }
            else
            {
                tblContact newData = new tblContact();
                newData = model;
                newData.ContactId = Guid.NewGuid();
                newData.CreatedBy = Session["User"].ToString();
                newData.CreatedDate = DateTime.Now;
                newData.IsActive = true;
                db.tblContacts.Add(newData);
                db.SaveChanges();
            }
            return RedirectToAction(MVC.Cms.Contact.Index());

        }
        public virtual ActionResult DeleteItem(Guid ids)
        {
            WorkWebsiteEntities db = new WorkWebsiteEntities();
            var data = tblContact.GetContactByID(ids);

            if (data != null)
            {
                data.IsActive = false;
                data.UpdatedBy = Session["User"].ToString();
                data.UpdatedDate = DateTime.Now;
                db.SaveChanges();
            }
            return RedirectToAction(MVC.Cms.Contact.Index());
        }
    }
}