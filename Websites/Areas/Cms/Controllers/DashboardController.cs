﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Websites.Areas.Cms.Controllers
{
    public partial class DashboardController : Controller
    {
        // GET: Cms/Dashboard
        public virtual ActionResult Index()
        {
            return View();
        }
    }
}