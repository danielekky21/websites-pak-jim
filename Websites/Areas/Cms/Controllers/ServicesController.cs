﻿using System;
using System.Collections.Generic;
using System.Linq;
using Websites.data;
using System.Web;
using System.Web.Mvc;
using System.IO;

namespace Websites.Areas.Cms.Controllers
{
    public partial class ServicesController : Controller
    {
        [SessionTimeout]
        // GET: Cms/Services
        public virtual ActionResult Index()
        {
            var data = tblServices.GetAllService().ToList();
            ViewData.Model = data;
            return View();
        }
        public virtual ActionResult AddEdit(Guid ids)
        {
            if (ids != Guid.Empty)
            {
                ViewData.Model = tblServices.GetServiceByID(ids);

            }
            else
            {
                ViewData.Model = new tblService();
            }
            return View();
        }
        [HttpPost]
        public virtual ActionResult AddEdit(tblService model, HttpPostedFileBase img)
        {
            WorkWebsiteEntities db = new WorkWebsiteEntities();
            try
            {


                if (model.ServicesID != Guid.Empty)
                {
                    var data = tblServices.GetServiceByID(model.ServicesID);
                    if (data != null)
                    {
                        data = model;
                        data.UpdatedBy = Session["User"].ToString();
                        data.UpdatedDate = DateTime.Now;
                        if (img != null)
                        {
                            string fileName = "";
                            string path = string.Empty;
                            string UploadPath = string.Empty;
                            string extension = string.Empty;
                            fileName = Guid.NewGuid().ToString();
                            UploadPath = "/Areas/cms/assets/img/";
                            if (img.ContentLength > 0)
                            {
                                extension = Path.GetExtension(img.FileName);
                                path = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(UploadPath), fileName + extension);
                                img.SaveAs(path);
                                data.ServiceImg = UploadPath + fileName + extension;
                            }
                        }
                        db.SaveChanges();
                    }
                }
                else
                {
                    tblService newData = new tblService();
                    newData = model;
                    newData.ServicesID = Guid.NewGuid();
                    newData.IsActive = true;
                    newData.CreatedBy = Session["User"].ToString();
                    newData.CreatedDate = DateTime.Now;
                    if (img != null)
                    {
                        string fileName = "";
                        string path = string.Empty;
                        string UploadPath = string.Empty;
                        string extension = string.Empty;
                        fileName = Guid.NewGuid().ToString();
                        UploadPath = "/Areas/cms/assets/img/";
                        if (img.ContentLength > 0)
                        {
                            extension = Path.GetExtension(img.FileName);
                            path = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(UploadPath), fileName + extension);
                            img.SaveAs(path);
                            newData.ServiceImg = UploadPath + fileName + extension;
                        }
                    }
                    db.tblServices.Add(newData);
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {

                throw;
            }
            return RedirectToAction(MVC.Cms.Services.Index());
        }
        public virtual ActionResult DeleteItem(Guid ids)
        {
            WorkWebsiteEntities db = new WorkWebsiteEntities();
            if (!string.IsNullOrEmpty(ids.ToString()))
            {
                var data = tblServices.GetServiceByID(ids);
                if (data != null)
                {
                    data.IsActive = false;
                    data.UpdatedBy = Session["User"].ToString();
                    data.UpdatedDate = DateTime.Now;
                    db.SaveChanges();
                }
            }
            return RedirectToAction(MVC.Cms.Services.Index());
        }
    }
}