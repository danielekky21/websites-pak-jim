﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Websites.data;
using System.Web.Mvc;

namespace Websites.Areas.Cms.Controllers
{
    public partial class HeadingController : Controller
    {
        // GET: Cms/Heading
        public virtual ActionResult Index()
        {
            WorkWebsiteEntities db = new WorkWebsiteEntities();
            var data = tblHeading.GetAllHeadingData().OrderByDescending(x => x.CreatedDate).ToList();
            ViewData.Model = data;
            return View();
        }
        public virtual ActionResult AddEdit(Guid ids)
        {
            if (ids != Guid.Empty)
            {
                ViewData.Model = data.tblHeading.GetHeadingByID(ids);
            }
            else
            {
                ViewData.Model = new tblHeading();
            }
            return View();
        }

        [HttpPost]
        public virtual ActionResult AddEdit(tblHeading model)
        {
            WorkWebsiteEntities db = new WorkWebsiteEntities();
            if (model.HeadingID != Guid.Empty)
            {

                var data = tblHeading.GetHeadingByID(model.HeadingID);
                data = model;
                data.UpdatedDate = DateTime.Now;
                data.UpdatedBy = Session["User"].ToString();
                db.SaveChanges();
            }
            else
            {
                var newData = new tblHeading();
                newData = model;
                newData.HeadingID = Guid.NewGuid();
                newData.IsActive = true;
                newData.CreatedBy = Session["User"].ToString();
                newData.CreatedDate = DateTime.Now;
                db.tblHeadings.Add(newData);                                                             
                db.SaveChanges();
            }
            return RedirectToAction(MVC.Cms.Heading.Index());
        }
        public virtual ActionResult DeleteItem(Guid ids)
        {
            WorkWebsiteEntities db = new WorkWebsiteEntities();
            if (!string.IsNullOrEmpty(ids.ToString()))
            {
                var data = tblHeading.GetHeadingByID(ids);
                if (data != null)
                {
                    data.UpdatedBy = Session["User"].ToString();
                    data.UpdatedDate = DateTime.Now;
                    data.IsActive = false;
                    db.SaveChanges();
                }
            }
            return RedirectToAction(MVC.Cms.Heading.Index());
        }

    }
}