﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Websites.data;

namespace Websites.Areas.Cms.Controllers
{
    public partial class AboutController : Controller
    {
        [SessionTimeout]
        public virtual ActionResult Index()
        {
            ViewData.Model = Websites.data.tblAbout.GetAllAbout().ToList();
            return View();
        }
        public virtual ActionResult AddEdit(Guid ids)
        {
            if (ids != Guid.Empty)
            {
                ViewData.Model = Websites.data.tblAbout.GetAboutByID(ids);
            }
            else
            {
                ViewData.Model = new Websites.data.tblAbout();

            }
            return View();
        }
        [HttpPost]
        public virtual ActionResult AddEdit(Websites.data.tblAbout model, HttpPostedFileBase img)
        {
            WorkWebsiteEntities db = new WorkWebsiteEntities();
            if (model.AboutID != Guid.Empty)
            {
                var data = tblAbout.GetAboutByID(model.AboutID);
                if (data != null)
                {

                    data = model;
                    data.UpdatedDate = DateTime.Now;
                    data.UpdatedBy = Session["User"].ToString();
                    if (img != null)
                    {
                        string fileName = "";
                        string path = string.Empty;
                        string UploadPath = string.Empty;
                        string extension = string.Empty;
                        fileName = Guid.NewGuid().ToString();
                        UploadPath = "/Areas/cms/assets/img/";
                        if (img.ContentLength > 0)
                        {
                            extension = Path.GetExtension(img.FileName);
                            path = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(UploadPath), fileName + extension);
                            img.SaveAs(path);
                            data.AboutImg1Path = UploadPath + fileName + extension;
                        }
                    }
                    db.SaveChanges();
                }
            }
            else
            {
                var newData = new tblAbout();
                newData = model;
                newData.AboutID = Guid.NewGuid();
                newData.CreatedBy = Session["User"].ToString();
                newData.CreatedDate = DateTime.Now;
                newData.IsActive = true;
                if (img != null)
                {
                    string fileName = "";
                    string path = string.Empty;
                    string UploadPath = string.Empty;
                    string extension = string.Empty;
                    fileName = Guid.NewGuid().ToString();
                    UploadPath = "/Areas/cms/assets/img/";
                    if (img.ContentLength > 0)
                    {
                        extension = Path.GetExtension(img.FileName);
                        path = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(UploadPath), fileName + extension);
                        img.SaveAs(path);
                        newData.AboutImg1Path = UploadPath + fileName + extension;
                    }
                }
                db.tblAbouts.Add(newData);
                db.SaveChanges();
            }
            return RedirectToAction(MVC.Cms.About.Index());
        }
        public virtual ActionResult DeleteItem(Guid ids)
        {
            WorkWebsiteEntities db = new WorkWebsiteEntities();
            if (ids != Guid.Empty)
            {
                var data = tblAbout.GetAboutByID(ids);
                if (data != null)
                {
                    data.IsActive = false;
                    data.UpdatedBy = Session["User"].ToString();
                    data.UpdatedDate = DateTime.Now;
                    db.SaveChanges();
                }
            }
            return RedirectToAction(MVC.Cms.About.Index());
        }
    }
}