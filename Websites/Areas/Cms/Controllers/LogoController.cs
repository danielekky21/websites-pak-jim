﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Websites.data;

namespace Websites.Areas.Cms.Controllers
{
    public partial class LogoController : Controller
    {
        // GET: Cms/Logo
        public virtual ActionResult Index()
        {
            ViewData.Model = tblLogo.GetAllLogo().OrderByDescending(x => x.LogoId).ToList();
            return View();
        }
        public virtual ActionResult AddEdit(Guid ids)
        {
            if (ids != Guid.Empty)
            {
                ViewData.Model = tblLogo.GetLogoByID(ids);
            }
            else
            {
                ViewData.Model = new tblLogo();
            }
            return View();
        }
        [HttpPost]
        public virtual ActionResult AddEdit(tblLogo Model, HttpPostedFileBase img)
        {
            WorkWebsiteEntities db = new WorkWebsiteEntities();
            if (Model.LogoId != Guid.Empty)
            {
                var data = tblLogo.GetLogoByID(Model.LogoId);
                if (data != null)
                {
               
                    data = Model;
                    data.UpdatedDate = DateTime.Now;
                    data.UpdatedBy = Session["User"].ToString();
                    if (img != null)
                    {
                        string fileName = "";
                        string path = string.Empty;
                        string UploadPath = string.Empty;
                        string extension = string.Empty;
                        fileName = Guid.NewGuid().ToString();
                        UploadPath = "/Areas/cms/assets/img/";
                        if (img.ContentLength > 0)
                        {
                            extension = Path.GetExtension(img.FileName);
                            path = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(UploadPath), fileName + extension);
                            img.SaveAs(path);
                            data.LogoPath = UploadPath + fileName + extension;
                        }
                    }
                    db.SaveChanges();
                }
            }
            else
            {
                var newData = new tblLogo();
                newData = Model;
                newData.LogoId = Guid.NewGuid();
                newData.CreatedBy = Session["User"].ToString();
                newData.CreatedDate = DateTime.Now;
                newData.IsActive = false;
                if (img != null)
                {
                    string fileName = "";
                    string path = string.Empty;
                    string UploadPath = string.Empty;
                    string extension = string.Empty;
                    fileName = Guid.NewGuid().ToString();
                    UploadPath = "/Areas/cms/assets/img/";
                    if (img.ContentLength > 0)
                    {
                        extension = Path.GetExtension(img.FileName);
                        path = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(UploadPath), fileName + extension);
                        img.SaveAs(path);
                        newData.LogoPath = UploadPath + fileName + extension;
                    }
                }
                db.tblLogoes.Add(newData);
                db.SaveChanges();
            }
            return RedirectToAction(MVC.Cms.Logo.Index());
        }
        public virtual ActionResult SetActive(Guid ids)
        {
            WorkWebsiteEntities db = new WorkWebsiteEntities();
            if (!string.IsNullOrEmpty(ids.ToString()))
            {
                var data = tblLogo.GetLogoByID(ids);
                if (data != null)
                {
                    data.IsActive = true;
                    data.UpdatedDate = DateTime.Now;
                    data.UpdatedBy = Session["User"].ToString();
                    db.SaveChanges();
                }
            }
            return RedirectToAction(MVC.Cms.Logo.Index());
        }
    }
}