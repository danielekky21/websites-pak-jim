﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using websites.security.security;
using Websites.data;
namespace Websites.Areas.Cms.Controllers
{
    public partial class LoginController : Controller
    {
        // GET: Cms/Login
        public virtual ActionResult Index(string username,string password)
        {
            var data = tblUser.GetByUserNamePassword(username, DataEncryption.Encrypt(password));
            if (data != null)
            {
                Session["User"] = data.UserName;
                return RedirectToAction(MVC.Cms.Dashboard.Index());
            }
            return View();
        }
        
    }
}