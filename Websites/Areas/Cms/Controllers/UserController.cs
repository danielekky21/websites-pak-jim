﻿using System;
using System.Collections.Generic;
using System.Linq;
using Websites.data;
using websites.security;
using System.Web;
using System.Web.Mvc;
using websites.security.security;

namespace Websites.Areas.Cms.Controllers
{
    public partial class UserController : Controller
    {
        // GET: Cms/User
        public virtual ActionResult Index()
        {
            ViewData.Model = tblUser.GetAllUser().ToList();
           
            return View();
        }
        public virtual ActionResult AddEdit(string ids)
        {
            if (!string.IsNullOrEmpty(ids))
            {
                ViewData.Model = tblUser.GetUserByUserName(ids);

            }
            else
            {
                ViewData.Model = new tblUser();
            }
            return View();
        }
        [HttpPost]
        public virtual ActionResult AddEdit(tblUser model)
        {
            WorkWebsiteEntities db = new WorkWebsiteEntities();
            if (!string.IsNullOrEmpty(model.UserName))
            {
                var data = tblUser.GetUserByUserName(model.UserName);
                if (data != null)
                {
                    data = model;
                    data.UpdatedBy = Session["User"].ToString();
                    data.UpdatedDate = DateTime.Now;
                    db.SaveChanges();
                }
                else
                {
                    tblUser newData = new tblUser();
                    newData = model;
                    newData.Password = DataEncryption.Encrypt(model.Password);
                    newData.CreatedBy = Session["User"].ToString();
                    newData.CreatedDate = DateTime.Now;
                    newData.IsActive = true;
                    db.tblUsers.Add(newData);
                    db.SaveChanges();
                }
            }
            return RedirectToAction(MVC.Cms.User.Index());
        }
        public virtual ActionResult DeleteUser(string username)
        {
            WorkWebsiteEntities db = new WorkWebsiteEntities();
            if (!string.IsNullOrEmpty(username.ToString()))
            {
                var data = tblUser.GetUserByUserName(username);
                if (data != null)
                {
                    data.IsActive = true;
                    data.UpdatedDate = DateTime.Now;
                    data.UpdatedBy = Session["User"].ToString();
                    db.SaveChanges();
                }
            }
            return RedirectToAction(MVC.Cms.User.Index());
        }

    }
}