﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Websites.data;

namespace Websites.Areas.Cms.Controllers
{
    public partial class HeadingPictureController : Controller
    {
        // GET: Cms/HeadingPicture
        public virtual ActionResult Index()
        {
            ViewData.Model = tblHeading.GetHeadingPicByHeadingId().ToList();
            return View();
        }
        public virtual ActionResult AddEdit(Guid ids)
        {
            if (ids != Guid.Empty)
            {
                ViewData.Model = tblHeading.GetHeadingPictByID(ids);

            }
            else
            {
                ViewData.Model = new tblHeadingPicture();
            }
            return View();
        }
        [HttpPost]
        public virtual ActionResult AddEdit(tblHeadingPicture Model, HttpPostedFileBase img)
        {
            WorkWebsiteEntities db = new WorkWebsiteEntities();
            if (Model.HeadingPictId != Guid.Empty)
            {
                var data = tblHeading.GetHeadingPictByID(Model.HeadingPictId);
                if (data != null)
                {

                    data = Model;
                    data.UpdatedDate = DateTime.Now;
                    data.UpdatedBy = Session["User"].ToString();
                    if (img != null)
                    {
                        string fileName = "";
                        string path = string.Empty;
                        string UploadPath = string.Empty;
                        string extension = string.Empty;
                        fileName = Guid.NewGuid().ToString();
                        UploadPath = "/Areas/cms/assets/img/";
                        if (img.ContentLength > 0)
                        {
                            extension = Path.GetExtension(img.FileName);
                            path = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(UploadPath), fileName + extension);
                            img.SaveAs(path);
                            data.Path = UploadPath + fileName + extension;
                        }
                    }
                    db.SaveChanges();
                }
                
            }
            else
            {
                var newData = new tblHeadingPicture();
                newData = Model;
                newData.CreatedBy = Session["User"].ToString();
                newData.CreatedDate = DateTime.Now;
                newData.IsActive = false;
                if (img != null)
                {
                    string fileName = "";
                    string path = string.Empty;
                    string UploadPath = string.Empty;
                    string extension = string.Empty;
                    fileName = Guid.NewGuid().ToString();
                    UploadPath = "/Areas/cms/assets/img/";
                    if (img.ContentLength > 0)
                    {
                        extension = Path.GetExtension(img.FileName);
                        path = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(UploadPath), fileName + extension);
                        img.SaveAs(path);
                        newData.Path = UploadPath + fileName + extension;
                    }
                }
                db.tblHeadingPictures.Add(newData);
                db.SaveChanges();
            }
            return RedirectToAction(MVC.Cms.HeadingPicture.Index());
        }
        public virtual ActionResult DeleteItem(Guid ids)
        {
            WorkWebsiteEntities db = new WorkWebsiteEntities();
            var data = tblHeading.GetHeadingPictByID(ids);

            if (data != null)
            {
                data.IsActive = false;
                data.UpdatedBy = Session["User"].ToString();
                data.UpdatedDate = DateTime.Now;
                db.SaveChanges();
            }
            return RedirectToAction(MVC.Cms.Contact.Index());
        }
    }
}